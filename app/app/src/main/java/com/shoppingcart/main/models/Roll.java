package com.shoppingcart.main.models;

import java.io.Serializable;

public class Roll implements Serializable {
    private final int mId;
    private String mName;
    private int mWeight;
    private int mPrice;
    private int mImgId;

    public Roll(int id, String name, int weight, int price, int imgId) {
        this.mId = id;
        this.mName = name;
        this.mWeight = weight;
        this.mPrice = price;
        this.mImgId = imgId;
    }

    public Roll(Roll other) {
        this.mId = other.mId;
        this.mName = other.mName;
        this.mWeight = other.mWeight;
        this.mPrice = other.mPrice;
        this.mImgId = other.mImgId;
    }

    public int getId() {
        return mId;
    }

    public int getPrice() {
        return mPrice;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public int getWeight() {
        return mWeight;
    }

    public void setWeight(int weight) {
        this.mWeight = weight;
    }

    public void setPrice(int price) {
        this.mPrice = price;
    }

    public int getImgId() {
        return mImgId;
    }

    public void setImgId(int mImgId) {
        this.mImgId = mImgId;
    }
}
