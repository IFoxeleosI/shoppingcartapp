package com.shoppingcart.main.models;

public class OrderRoll extends Roll {
    private int mCount = 0;

    public OrderRoll(int id, String name, int weight, int price, int imgId) {
        super(id, name, weight, price, imgId);
    }

    public OrderRoll(int id, String name, int weight, int price, int imgId, int count) {
        super(id, name, weight, price, imgId);

        this.mCount = count;
    }

    public OrderRoll(Roll roll, int count) {
        super(roll);

        this.mCount = count;
    }

    public int getCount() {
        return mCount;
    }

    public void setCount(int mCount) {
        this.mCount = mCount;
    }
}
