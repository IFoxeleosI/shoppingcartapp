package com.shoppingcart.main.managers;

import com.shoppingcart.main.models.OrderRoll;
import com.shoppingcart.main.models.Roll;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OrderManager {
    public final int MAX_ORDER_SIZE = 8;
    private final RollsManager rollsManager;

    private final ArrayList<Integer> order = new ArrayList<Integer>(0);

    public OrderManager(RollsManager rollsManager) {
        this.rollsManager = rollsManager;
    }

    public ArrayList<Integer> getOrder() {
        return order;
    }

    public int getOrderPrice() {
        int result = 0;

        for (int rollId : order) {
            Roll currentRoll = rollsManager.getRollById(rollId);

            if (currentRoll != null) {
                result += currentRoll.getPrice();
            }
        }

        return result;
    }

    public int getOrderWeight() {
        int result = 0;

        for (int rollId : order) {
            Roll currentRoll = rollsManager.getRollById(rollId);

            if (currentRoll != null) {
                result += currentRoll.getWeight();
            }
        }

        return result;
    }

    public int getOrderAllCount() {
        return order.size();
    }

    public void addRollToOrder(int id) {
        order.add(id);
    }

    public void removeRollFromOrder(int id) {
        int currentOrderSize = order.size();
        Roll roll = rollsManager.getRollById(id);

        for (int i = currentOrderSize - 1; i >= 0; i--) {
            int currentRollId = order.get(i);

            if (currentRollId == roll.getId()) {
                order.remove(i);
                break;
            }
        }
    }

    public int getOrderedRollCountById(int id) {
        int result = 0;

        for (int rollId : order) {
            if (rollId == id) {
                result++;
            }
        }

        return result;
    }

    public ArrayList<OrderRoll> collectOrder() {
        HashMap<Integer, Integer> orderMap = new HashMap<>();

        for (int id : order) {
            if (!orderMap.containsKey(id)) {
                orderMap.put(id, 1);
            } else {
                int currentValue = orderMap.get(id);
                orderMap.put(id, currentValue + 1);
            }
        }

        ArrayList<OrderRoll> collectedOrderList = new ArrayList<OrderRoll>(0);

        for (Map.Entry<Integer, Integer> entry : orderMap.entrySet()) {
            int rollId = entry.getKey();
            int rollCount = entry.getValue();

            Roll currentRoll = rollsManager.getRollById(rollId);
            OrderRoll orderRoll = new OrderRoll(currentRoll, rollCount);
            collectedOrderList.add(orderRoll);
        }

        return collectedOrderList;
    }

    public void clearOrder() {
        order.clear();
    }

    public void unpackOrder(ArrayList<OrderRoll> collectedOrder) {
        order.clear();

        for (OrderRoll orderRoll : collectedOrder) {
            for (int i = 0; i < orderRoll.getCount(); i++) {
                order.add(orderRoll.getId());
            }
        }
    }
}
