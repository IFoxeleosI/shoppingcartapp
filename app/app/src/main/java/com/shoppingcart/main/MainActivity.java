package com.shoppingcart.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shoppingcart.main.managers.OrderManager;
import com.shoppingcart.main.managers.RollsManager;
import com.shoppingcart.main.models.OrderRoll;
import com.shoppingcart.main.models.Roll;

import java.util.ArrayList;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    public static final String CURRENT_ORDER_KEY = "com.shoppingcart.main.CURRENT_ORDER";
    private RollsManager rollsManager;
    private OrderManager orderManager;
    private int selectedRollIndex = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = findViewById(R.id.custom_toolbar);
        setSupportActionBar(myToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        rollsManager = new RollsManager(this, "menu.json");
        orderManager = new OrderManager(rollsManager);

        ImageButton addButton = findViewById(R.id.roll_preview_add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (orderManager.getOrderAllCount() == orderManager.MAX_ORDER_SIZE) {
                    String toastText = getString(R.string.main_max_size_text);

                    Toast.makeText(MainActivity.this, toastText, Toast.LENGTH_LONG).show();
                }

                if (rollsManager.getOrderedRollsIdList().size() > 0 && orderManager.getOrderAllCount() < orderManager.MAX_ORDER_SIZE) {
                    Roll roll = getSelectedRoll();
                    orderManager.addRollToOrder(roll.getId());
                    updateOrderView();
                }
            }
        });

        ImageButton removeButton = findViewById(R.id.roll_preview_remove_button);
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rollsManager.getOrderedRollsIdList().size() > 0) {
                    Roll roll = getSelectedRoll();

                    orderManager.removeRollFromOrder(roll.getId());

                    updateOrderView();
                }
            }
        });

        ImageButton prevButton = findViewById(R.id.roll_preview_prev_button);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedRollIndex =
                        (rollsManager.getOrderedRollsIdList().size() + selectedRollIndex - 1)
                        % rollsManager.getOrderedRollsIdList().size();

                updateSelectedRollView();
            }
        });

        ImageButton nextButton = findViewById(R.id.roll_preview_next_button);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedRollIndex = (selectedRollIndex + 1) % rollsManager.getOrderedRollsIdList().size();

                updateSelectedRollView();
            }
        });

        ImageButton basketButton = findViewById(R.id.cart_button);
        basketButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCart();
            }
        });

        if (rollsManager.getOrderedRollsIdList().size() > 0) {
            selectedRollIndex = 0;
        }

        updateSelectedRollView();
    }

    public void goToCart() {
        Intent intent = new Intent(this, CartActivity.class);
        Bundle bundle = new Bundle();

        ArrayList<OrderRoll> collectedOrder = orderManager.collectOrder();

        bundle.putSerializable(CURRENT_ORDER_KEY, collectedOrder);
        intent.putExtras(bundle);

        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null) {
            return;
        }

        if (resultCode == Activity.RESULT_OK) {
            Boolean shouldBeCleared = data.getBooleanExtra("shouldBeCleared", false);

            if (shouldBeCleared) {
                orderManager.clearOrder();
                updateOrderView();
            }
        }
    }

    public Roll getSelectedRoll() {
        if (rollsManager.getOrderedRollsIdList().size() <= selectedRollIndex || selectedRollIndex < 0) {
            return null;
        }

        int selectedRollId = rollsManager.getOrderedRollsIdList().get(selectedRollIndex);

        return rollsManager.getRollById(selectedRollId);
    }

    public String printSelectedRoll() {
        Roll selectedRoll = getSelectedRoll();

        if (selectedRoll == null) {
            return "Какой-то ролл";
        }

        return selectedRoll.getName();
    }

    public void updateOrderImagePreview() {
        ArrayList<Integer> order = orderManager.getOrder();

        for (int i = 0; i < orderManager.MAX_ORDER_SIZE; ++i) {
            String cellId = "order_preview_cell_" + Integer.toString(i + 1);
            ImageView imageCell = findViewById(getResources().getIdentifier(cellId, "id", getPackageName()));

            if (imageCell != null) {
                if (orderManager.getOrderAllCount() > i) {
                    Integer currentRollId = order.get(i);
                    Drawable imageDrawable = rollsManager.getRollImageById(currentRollId);
                    imageCell.setImageDrawable(imageDrawable);
                } else {
                    imageCell.setImageResource(0);
                }
            }
        }
    }

    public void updateOrderView() {
        TextView priceTextView = findViewById(R.id.toolbar_price_text_view);
        TextView weightTextView = findViewById(R.id.toolbar_weight_text_view);
        TextView orderedRollCountTextView = findViewById(R.id.roll_preview_current_count);

        String priceText = Integer.toString(orderManager.getOrderPrice());
        String weightText = Integer.toString(orderManager.getOrderWeight());

        Roll selectedRoll = getSelectedRoll();
        String countOrderText = Integer.toString(orderManager.getOrderedRollCountById(selectedRoll.getId()));

        priceTextView.setText(priceText);
        weightTextView.setText(weightText);
        orderedRollCountTextView.setText(countOrderText);
        updateOrderImagePreview();
    }

    public void updateSelectedRollView() {
        TextView selectedRollTextView = findViewById(R.id.roll_preview_text);

        selectedRollTextView.setText(printSelectedRoll());

        ImageView selectedRollImageView = findViewById(R.id.roll_preview_image);

        Roll selectedRoll = getSelectedRoll();

        if (selectedRoll != null) {
            Drawable selectedRollDrawable = rollsManager.getRollImageById(selectedRoll.getId());

            if (selectedRollDrawable != null) {
                selectedRollImageView.setImageDrawable(rollsManager.getRollImageById(selectedRoll.getId()));
            }

            TextView orderedRollCountTextView = findViewById(R.id.roll_preview_current_count);
            String countOrderText = Integer.toString(orderManager.getOrderedRollCountById(selectedRoll.getId()));
            orderedRollCountTextView.setText(countOrderText);
        }
    }

}