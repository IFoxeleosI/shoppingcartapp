package com.shoppingcart.main.adapters;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import com.shoppingcart.main.R;
import com.shoppingcart.main.models.OrderRoll;
import com.shoppingcart.main.models.Roll;

import java.util.ArrayList;

public class RollViewAdapter extends ArrayAdapter<OrderRoll> {
    private final AppCompatActivity context;
    private final ArrayList<OrderRoll> rolls;

    static class ViewHolder {
        public TextView title;
        public ImageView imageView;
        public TextView count;
    }

    public RollViewAdapter(AppCompatActivity context, ArrayList<OrderRoll> rolls) {
        super(context, R.layout.cart_list_item, rolls);
        this.context = context;
        this.rolls = rolls;
    }

    public Drawable getRollImageById(int id) {
        if (id < 0) {
            return null;
        }

        String uri = "@drawable/roll_image_" + id;

        int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());

        return ResourcesCompat.getDrawable(context.getResources(), imageResource, null);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.cart_list_item, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.title = (TextView) rowView.findViewById(R.id.cart_list_item_title);
            viewHolder.imageView = (ImageView) rowView.findViewById(R.id.cart_list_item_image_view);
            viewHolder.count = (TextView) rowView.findViewById(R.id.cart_list_item_count);

            rowView.setTag(viewHolder);
        }

        // fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        OrderRoll r = rolls.get(position);
        holder.title.setText(r.getName());
        holder.count.setText(Integer.toString(r.getCount()));
        holder.imageView.setImageDrawable(getRollImageById(r.getId()));

        return rowView;
    }
}
