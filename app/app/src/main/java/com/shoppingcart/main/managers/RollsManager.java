package com.shoppingcart.main.managers;

import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import com.shoppingcart.main.models.Roll;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class RollsManager {
    private final String JSON_FIELD_ROLLS_ARRAY = "rolls";
    private final String JSON_FIELD_NAME = "name";
    private final String JSON_FIELD_ID = "id";
    private final String JSON_FIELD_PRICE = "price";
    private final String JSON_FIELD_WEIGHT = "weight";
    private final String JSON_FIELD_IMG_ID = "img_id";

    private final String IMG_PATH_PREFIX = "@drawable/roll_image_";
    private final String IMG_DEFAULT_IMG = "@drawable/cross";

    private final AppCompatActivity context;
    private final HashMap<Integer, Roll> rollsMap = new HashMap<Integer, Roll>(0);
    private final ArrayList<Integer> orderedRollsIdList = new ArrayList<Integer>(0);

    public RollsManager(AppCompatActivity context) {
        this.context = context;
    }

    public RollsManager(AppCompatActivity context, String filename) {
        this.context = context;
        loadMenu(filename);
    }

    public HashMap<Integer, Roll> getRollsMap() {
        return rollsMap;
    }

    public ArrayList<Integer> getOrderedRollsIdList() {
        return orderedRollsIdList;
    }

    public Roll getRollById(int id) {
        return rollsMap.get(id);
    }

    public Drawable getRollImageById(int id) {
        if (id < 0) {
            return null;
        }

        Resources resources = context.getResources();
        String packageName = context.getPackageName();

        Roll roll = rollsMap.get(id);

        if (roll == null) {
            return ResourcesCompat.getDrawable(resources, resources.getIdentifier(IMG_DEFAULT_IMG, null, packageName), null);
        }

        String uri = IMG_PATH_PREFIX + roll.getImgId();

        int imageResource = resources.getIdentifier(uri, null, packageName);

        return ResourcesCompat.getDrawable(resources, imageResource, null);
    }

    public void loadMenu(String filename) {
        String data = null;
        AssetManager am = context.getAssets();
        try {
            InputStream is = am.open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            data = new String(buffer, StandardCharsets.UTF_8);

            JSONObject jsonMenu = new JSONObject(data);
            JSONArray jsonRollsArray = jsonMenu.getJSONArray(JSON_FIELD_ROLLS_ARRAY);

            for (int i = 0; i < jsonRollsArray.length(); ++i) {
                JSONObject jsonRoll = jsonRollsArray.getJSONObject(i);
                int id = jsonRoll.getInt(JSON_FIELD_ID);

                if (!rollsMap.containsKey(id)) {
                    String name = jsonRoll.getString(JSON_FIELD_NAME);
                    int weight = jsonRoll.getInt(JSON_FIELD_WEIGHT);
                    int price = jsonRoll.getInt(JSON_FIELD_PRICE);
                    int imgId = jsonRoll.getInt(JSON_FIELD_IMG_ID);

                    Roll roll = new Roll(id, name, weight, price, imgId);

                    rollsMap.put(id, roll);
                    orderedRollsIdList.add(id);
                }
            }

            Collections.sort(orderedRollsIdList);
        } catch (IOException | JSONException ex) {
            ex.printStackTrace();
        }
    }
}
