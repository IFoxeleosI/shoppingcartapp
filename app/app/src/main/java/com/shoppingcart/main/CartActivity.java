package com.shoppingcart.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.shoppingcart.main.adapters.RollViewAdapter;
import com.shoppingcart.main.managers.OrderManager;
import com.shoppingcart.main.managers.RollsManager;
import com.shoppingcart.main.models.OrderRoll;

import java.util.ArrayList;

public class CartActivity extends AppCompatActivity {
    private RollsManager rollsManager;
    private OrderManager orderManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        ArrayList<OrderRoll> storedOrder = (ArrayList<OrderRoll>) bundle.getSerializable(MainActivity.CURRENT_ORDER_KEY);

        rollsManager = new RollsManager(this, "menu.json");
        orderManager = new OrderManager(rollsManager);
        orderManager.unpackOrder(storedOrder);

        ListView cartListView = findViewById(R.id.cart_order_items_list);

        RollViewAdapter arrayAdapter = new RollViewAdapter(this, storedOrder);

        cartListView.setAdapter(arrayAdapter);

        TextView priceTextView = findViewById(R.id.cart_info_price_text);
        TextView priceWeightView = findViewById(R.id.cart_info_weight_text);

        priceTextView.setText(Integer.toString(orderManager.getOrderPrice()));
        priceWeightView.setText(Integer.toString(orderManager.getOrderWeight()));

        ImageButton backButton = findViewById(R.id.cart_back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnToMain(false);
            }
        });

        Button clearButton = findViewById(R.id.cart_clear_button);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String toastText = getString(R.string.cart_toast_clear_text);

                Toast.makeText(CartActivity.this, toastText, Toast.LENGTH_SHORT).show();

                returnToMain(true);
            }
        });

        Button payButton = findViewById(R.id.cart_pay_button);
        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String toastText = getString(R.string.cart_toast_pay_text);

                Toast.makeText(CartActivity.this, toastText, Toast.LENGTH_SHORT).show();

                returnToMain(true);
            }
        });
    }

    public void returnToMain(Boolean shouldBeCleared) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("shouldBeCleared", shouldBeCleared);

        setResult(RESULT_OK, intent);
        finish();
    }

}
